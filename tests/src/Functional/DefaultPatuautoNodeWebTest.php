<?php

namespace Drupal\Tests\token_alias_d8\Functional;

use Drupal\Tests\pathauto\Functional\PathautoNodeWebTest;

/**
 * Class DefaultPathautoNodeWebTest.
 *
 * @package Drupal\Tests\token_alias_d8\Functional
 */
class DefaultPathautoNodeWebTest extends PathautoNodeWebTest {

  /**
   * {@inheritdoc}
   */
  function setUp() {
    self::$modules = array_merge(parent::$modules, ['token_alias_d8']);
    parent::setUp();
  }

}
