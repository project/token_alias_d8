<?php

namespace Drupal\Tests\token_alias_d8\Functional;

use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\pathauto\PathautoState;
use Drupal\system\Entity\Menu;
use Drupal\Tests\pathauto\Functional\PathautoTestHelperTrait;
use Drupal\user\Entity\Role;

/**
 * Class TokenAliasTranslationTest.
 *
 * @package Drupal\Tests\token_alias_d8\Functional
 */
class TokenAliasTranslationTestBase extends TokenAliasTestBase {

  use PathautoTestHelperTrait;

  protected $nodeType;

  /**
   * The pattern.
   *
   * @var \Drupal\pathauto\PathautoPatternInterface
   */
  protected $pattern;

  /**
   * @var \Drupal\system\MenuInterface
   */
  protected $menu;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    self::$modules = array_merge(parent::$modules, ['menu_ui', 'mhi_language', 'content_translation']);

    parent::setUp();

    // Create node type.
    $this->nodeType = $this->createNodeType('page', 'Basic page');

    $this->grantUserPermissionToCreateContentOfType($this->adminUser, $this->nodeType->id());
    $account = $this->adminUser;
    $role_ids = $account->getRoles(TRUE);
    /* @var \Drupal\user\RoleInterface $role */
    $role_id = reset($role_ids);
    $role = Role::load($role_id);
    $role->grantPermission('administer content translation');
    $role->grantPermission('translate any entity');
    $role->grantPermission('administer languages');
    $role->save();

    $this->drupalLogin($this->adminUser);

    // Create alias pattern.
    $this->pattern = $this->createPattern('node', '/[node:title].html');

    // Enable Translation of Basic Page
    $this->drupalGet('/admin/config/regional/content-language');
    $this->assertSession()->pageTextContains('Content language');
    $this->drupalPostForm(NULL, [
        "edit-entity-types-node" => "node",
        "edit-settings-node-page-translatable" => "1",
        "edit-settings-node-page-settings-language-language-alterable" => "1",
        "edit-settings-path-alias-path-alias-settings-language-language-alterable" => "1",
    ], $this->t("Save configuration"));

  }

  protected function createTranslation($node, $post) {
    $this->drupalGet("node/{$node->id()}/translations/add/en/ja");
    $this->drupalPostForm(NULL, $post, $this->t('Save'));
  }

}
