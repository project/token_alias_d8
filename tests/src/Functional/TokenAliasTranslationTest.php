<?php

namespace Drupal\Tests\token_alias_d8\Functional;

use Drupal\menu_link_content\Entity\MenuLinkContent;
use Drupal\pathauto\PathautoState;
use Drupal\system\Entity\Menu;
use Drupal\Tests\pathauto\Functional\PathautoTestHelperTrait;
use Drupal\user\Entity\Role;

/**
 * Class TokenAliasTranslationTest.
 *
 * @package Drupal\Tests\token_alias_d8\Functional
 */
class TokenAliasTranslationTest extends TokenAliasTranslationTestBase {

  use PathautoTestHelperTrait;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
  }

  /**
   * Test skip alias validation if the alias generated automatically
   *
   * @scenario 1
   * - On node add form
   *   - create the first node
   *   - uncheck the "Generate automatic URL alias" checkbox
   *   - fill the title and token alias manually
   *   - select language in English
   *   - save the node
   *
   * - On node add form
   *   - create the second node
   *   - uncheck the "Generate automatic URL alias" checkbox
   *   - fill the title and token alias manually, make the title and alias as same as the first node
   *   - select language in Janpanese
   *   - save the node
   *
   * - On node translate form
   *   - translate the first node to Janpanese
   *   - fill the title as same as the first node
   *   - if "Generate automatic URL alias" unchecked error message should show.
   *   - if "Generate automatic URL alias" checked error message should not show.
   *   - save the node
   *
   * - Result
   *   - all nodes created successfully
   *   - for the third node(the translate node),
   *     the alias generated automatically will be the same as the second node,
   *     but the conflict will be solved since it's automatically generated
   *
   * @scenario 2
   * - Note
   *   - The only defference between scenario 1 and scenario 2 is that note_1 and node_2 have the same title.
   *
   * - On node add form
   *   - create the first node
   *   - uncheck the "Generate automatic URL alias" checkbox
   *   - fill the title and token alias manually
   *   - select language in English
   *   - save the node
   *
   * - On node add form
   *   - create the second node
   *   - uncheck the "Generate automatic URL alias" checkbox
   *   - fill the title and token alias manually, make the title and alias as same as the first node
   *   - select language in Janpanese
   *   - save the node
   *
   * - On node translate form
   *   - translate the first node to Janpanese
   *   - fill the title, make it different from the first node
   *   - if "Generate automatic URL alias" unchecked error message should show.
   *   - if "Generate automatic URL alias" checked error message should not show.
   *   - save the node
   *
   * - Result
   *   - all nodes created successfully
   *   - for the third node(the translate node),
   *     the alias generated automatically will be the same as the second node,
   *     but the conflict will be solved since it's automatically generated
   *
   * - For more Information
   *   - https://jiracloud.cit.com.br/browse/MHIS-1503
   */
  public function testSkipValidation() {
    // scenario 1
    $title = 'title';
    $token_alias_d8 = '/title.html';
    $translate_token_alias_d8 = '/title.html-0';

    // Create the first node
    $post = [
      'title[0][value]' => $title,
      'path[0][pathauto]' => PathautoState::SKIP,
      'path[0][alias]' => $token_alias_d8,
    ];
    $node_1 = $this->createNodePage($post);
    $this->drupalGet("node/{$node_1->id()}/edit");
    // validate node
    $this->assertSession()->fieldValueEquals('title[0][value]', $title);
    $this->assertSession()->fieldValueEquals('path[0][alias]', $token_alias_d8);
    $this->assertSession()->checkboxNotChecked('edit-path-0-pathauto');

    // Create second node.
    $post = [
      'title[0][value]' => $title,
      'path[0][pathauto]' => PathautoState::SKIP,
      'path[0][alias]' => $token_alias_d8,
      'langcode[0][value]' => 'ja',
    ];
    $node_2 = $this->createNodePage($post);
    $this->drupalGet("node/{$node_2->id()}/edit");
    // validate node
    $this->assertSession()->fieldValueEquals('title[0][value]', $title);
    $this->assertSession()->fieldValueEquals('path[0][alias]', $token_alias_d8);
    $this->assertSession()->checkboxNotChecked('edit-path-0-pathauto');

    // Create the translate node of first node
    $post = [
      'title[0][value]' => $title,
      'path[0][alias]' => $token_alias_d8,
      'path[0][pathauto]' => PathautoState::SKIP,
    ];
    // If "Generate automatic URL alias" unchecked, error message should show.
    $this->createTranslation($node_1, $post);
    $this->assertSession()->pageTextContains("The alias " . $token_alias_d8 . " is already in use in this language.");
    // If "Generate automatic URL alias" checked, error message should not show.
    $post['path[0][pathauto]'] = PathautoState::CREATE;
    $this->createTranslation($node_1, $post);
    $this->assertSession()->pageTextNotContains("The alias " . $token_alias_d8 . " is already in use in this language.");
    // validate node
    $this->drupalGet("jp/node/{$node_1->id()}/edit");
    $this->assertSession()->fieldValueEquals('title[0][value]', $title);
    $this->assertSession()->fieldValueEquals('path[0][alias]', $translate_token_alias_d8);
    $this->assertSession()->checkboxChecked('edit-path-0-pathauto');

   // scenario 2
    $title = 'title2';
    $title2 = 'title2-2';
    $token_alias_d8 = '/title2.html';
    $translate_token_alias_d8 = '/title2-2.html';

    // Create the first node
    $post = [
      'title[0][value]' => $title,
      'path[0][pathauto]' => PathautoState::SKIP,
      'path[0][alias]' => $token_alias_d8,
    ];
    $node_1 = $this->createNodePage($post);
    $this->drupalGet("node/{$node_1->id()}/edit");
    // validate node
    $this->assertSession()->fieldValueEquals('title[0][value]', $title);
    $this->assertSession()->fieldValueEquals('path[0][alias]', $token_alias_d8);
    $this->assertSession()->checkboxNotChecked('edit-path-0-pathauto');

    // Create second node.
    $post = [
      'title[0][value]' => $title,
      'path[0][pathauto]' => PathautoState::SKIP,
      'path[0][alias]' => $token_alias_d8,
      'langcode[0][value]' => "ja",
    ];
    $node_2 = $this->createNodePage($post);
    $this->drupalGet("node/{$node_2->id()}/edit");
    // validate node
    $this->assertSession()->fieldValueEquals('title[0][value]', $title);
    $this->assertSession()->fieldValueEquals('path[0][alias]', $token_alias_d8);
    $this->assertSession()->checkboxNotChecked('edit-path-0-pathauto');

    // Create the translate node of first node
    $post = [
      'title[0][value]' => $title2,
      'path[0][alias]' => $token_alias_d8,
      'path[0][pathauto]' => PathautoState::SKIP,
    ];
    // If "Generate automatic URL alias" unchecked, error message should show.
    $this->createTranslation($node_1, $post);
    $this->assertSession()->pageTextContains("The alias " . $token_alias_d8 . " is already in use in this language.");
    // If "Generate automatic URL alias" checked, error message should not show.
    $post['path[0][pathauto]'] = PathautoState::CREATE;
    $this->createTranslation($node_1, $post);
    $this->assertSession()->pageTextNotContains("The alias " . $token_alias_d8 . " is already in use in this language.");
    // validate node
    $this->drupalGet("jp/node/{$node_1->id()}/edit");
    $this->assertSession()->fieldValueEquals('title[0][value]', $title2);
    $this->assertSession()->fieldValueEquals('path[0][alias]', $translate_token_alias_d8);
    $this->assertSession()->checkboxChecked('edit-path-0-pathauto');
  }
}
